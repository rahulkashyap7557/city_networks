/*
    Copyright (c) Rahul Kashyap 2023

    This file is part of CITY_NETWORK.

    CITY_NETWORK is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    CITY_NETWORK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CITY_NETWORK.  If not, see <http://www.gnu.org/licenses/>.

*/
#pragma once
#include <iostream>
#include <map>
#include <vector>
#include <functional>
#include <fstream>
#include <string>
#include <random>

using NodeId = int;
using CellId = int;
enum struct NodeType {
    PERSON = 1,
    COMMUTE_POINT,
    WORK,
    UTILITY_BUSINESS, // can be something like a restaurant, theaters, etc.
    NECESSARY_BUSINESS // can be something like doctors, grocery stores, pharmacies
};

// Point class for storing coordinates
struct Point {
    double x;
    double y;

    Point operator +(const Point& otherPoint) {
        Point outPoint;
        outPoint.x = x + otherPoint.x;
        outPoint.y = y + otherPoint.y;

        return outPoint;
    }

    Point operator -(const Point& otherPoint) {
        Point outPoint;
        outPoint.x = x - otherPoint.x;
        outPoint.y = y - otherPoint.y;

        return outPoint;
    }

    double getMagnitudeSquared() const {
        return (x * x + y * y);
    }
};

// Each node represents a NodeType/entity and its parameters
class Node {
public:
    auto getLocation() const { return this->_location; }
    auto getNodeId() const { return this->_nodeId; }
    void setNodeId(NodeId nodeId) { this->_nodeId = nodeId; }
    auto getCellId() const { return this->_cellId; }
    void setCellId(CellId cellId) { this->_cellId = cellId; }
    void setLocation(Point& location) { this->_location = location; }
    double getAssociationRadius() const { return this->_associationRadius; }
    void setAssociationRadius(double associationRadius) { this->_associationRadius = associationRadius; }
    void setNodeType(NodeType nodeType) { this->_type = nodeType; }
private:
    NodeType _type = NodeType::PERSON;
    Point _location{ 0., 0. };
    double _associationRadius = 0.0;    
    NodeId _nodeId = 0;
    CellId _cellId = 0;
};

// Lattice points contain multiple entities/nodes
using NodeList = std::vector<Node>;
class LatticePoint {
public:
    auto getEntities() const { return this->_entitiesOnLatticePoint; }
    void setCellSize(double cellSize) { this->_cellSize = cellSize; }
    void setCellId(CellId cellId) { this->_cellId = cellId; }
    void addEntity(Node& node) { this->_entitiesOnLatticePoint.push_back(node); }
private:    
    NodeList _entitiesOnLatticePoint;
    double _cellSize = 1.0;
    CellId _cellId = 0;
};

bool nodesWithinRadius(const Node& sourceNode, const Node& targetNode, const double associationRadius) {
    double associationRadiusSq = associationRadius * associationRadius;
    Point displacement = targetNode.getLocation() - sourceNode.getLocation();
    double displacementMagnitudeSq = displacement.getMagnitudeSquared();
    if (displacementMagnitudeSq < associationRadiusSq) {
        return true;
    }
    else {
        return false;
    }
}

// This is the graph class
// We have a list of nodes. Depending on their association radius, we can generate the connections
class Lattice {
public:
    using EntityList = std::vector<LatticePoint>;
    Lattice(int numRows, int numCols, double cellSize) {
        this->_numRows = numRows;
        this->_numCols = numCols;
        this->_cellSize = cellSize;
    }

    void buildEntityList(LatticePoint& latticePoint) { 
        this->_allEntities.push_back(latticePoint);
        for (auto const& node : latticePoint.getEntities()) {
            this->_allNodes.push_back(node);
        }
    }
    auto getEntityList() const { return this->_allEntities; }
    auto getNodeList() const { return this->_allNodes; }
    auto getAdjacencyList() const { return this->_adjacencyList; }
    void generateAssociations(){
        for (auto const& node : this->_allNodes) {
            for (auto const& targetNode : this->_allNodes) {
                if (node.getNodeId() != targetNode.getNodeId() && nodesWithinRadius(node, targetNode, node.getAssociationRadius())) {
                    this->_adjacencyList[node].push_back(targetNode);
                }
            }
        }
    }
private:    
    // contains the clusters in each cell
    EntityList _allEntities;
    NodeList _allNodes;
    // For each node (NOT lattice point)s in the lattice, how is it connected to other nearby nodes?
    // Note that it may happen only some nodes from a lattice point may be included in the associations
    // This depends on the association radius and the disorder parameter
    using NodeMap = std::map < Node, NodeList, decltype([](const Node& a, const Node& b) {return a.getNodeId() < b.getNodeId(); }) > ;
    NodeMap _adjacencyList;

    int _numRows = 1;
    int _numCols = 1;
    double _cellSize = 1.0;
};

void calculateOrder(Lattice& lattice) {
    return;
}

Node createNode(double xLoc, double yLoc, double associationRadius,NodeId nodeId, CellId cellId, NodeType nodeType) {
    Node node;
    node.setAssociationRadius(associationRadius);
    Point location = { xLoc, yLoc };
    node.setLocation(location);
    node.setNodeType(nodeType);
    node.setNodeId(nodeId);
    node.setCellId(cellId);

    return node;
}

// Generate rows x cols grid and create a list of points
//
auto generateGrid(int rows, int cols, double cellSize, double associationRadius, double disorderParameter) {
    int nodeId = 0;
    int cellId = 0;
    Lattice lattice(rows, cols, cellSize);
    for (int ii = 0; ii < rows; ii++) {
        for (int jj = 0; jj < cols; jj++) {
            LatticePoint latticePoint;
            latticePoint.setCellSize(cellSize);
            double xLoc = (ii + 0.5) * cellSize;
            double yLoc = (jj + 0.5) + cellSize;

            if (disorderParameter > 0.00001) {
                std::random_device rd; // obtain a random number from hardware
                std::mt19937 gen(rd()); // seed the generator
                std::uniform_real_distribution<double> distr(-disorderParameter, disorderParameter); // define the range
                xLoc += distr(gen);
                yLoc += distr(gen);
            }

            // add a probability - 60%
            Node nodePerson = createNode(xLoc, yLoc, associationRadius, nodeId, cellId, NodeType::PERSON);    
            latticePoint.addEntity(nodePerson);
            nodeId++;

            //// add a probability - 30%
            //Node nodeCommute = createNode(xLoc, yLoc, associationRadius, nodeId, cellId, NodeType::COMMUTE_POINT);
            //latticePoint.addEntity(nodeCommute);
            //nodeId++;

            //// add a probability - 40%
            //Node nodeNecessaryBusiness = createNode(xLoc, yLoc, associationRadius, nodeId, cellId, NodeType::NECESSARY_BUSINESS);
            //latticePoint.addEntity(nodeNecessaryBusiness);
            //nodeId++;

            //// add a probability - 30%
            //Node nodeUtilityBusiness = createNode(xLoc, yLoc, associationRadius, nodeId, cellId, NodeType::UTILITY_BUSINESS);
            //latticePoint.addEntity(nodeUtilityBusiness);
            //nodeId++;

            //// add a probability - 30%
            //Node nodeWork = createNode(xLoc, yLoc, associationRadius, nodeId, cellId, NodeType::WORK);
            //latticePoint.addEntity(nodeWork);
            //nodeId++;

            cellId++;
            latticePoint.setCellSize(cellSize);
            lattice.buildEntityList(latticePoint);
        }
    }

    lattice.generateAssociations();
    return lattice;
}

Lattice generateOrderedLattice() {
    // Create a grid of points
    int numRows = 10;
    int numCols = 10;
    double cellSize = 1.0;
    double associationRadius = 2.0;
    // if there is a need to create disorder create it.
    double disorderParameter = 0.0;
    auto gridOfPoints = generateGrid(numRows, numCols, cellSize, associationRadius, disorderParameter);



    // Create a set of entities. We need atleast as many as rows x cols in total
    // Generate 70% of rows x cols of people
    // Generate 20% of commute (maybe)
    // Generate 40% work places
    // Generate 50% of necessary
    // Generate 30% of optional
    // For each of these entities, assign to a randomly selected cell from the list of points
    

    // Once the points have been assigned, create the edges based on the association radius
    // to fill out the adjacency list

    // Now the lattice is complete.

    return gridOfPoints;
}

Lattice generateDisorderedLattice() {
    int numRows = 5;
    int numCols = 5;
    double cellSize = 2.0;
    double associationRadius = 2.0;
    // if there is a need to create disorder create it.
    double disorderParameter = 0.5;
    auto gridOfPoints = generateGrid(numRows, numCols, cellSize, associationRadius, disorderParameter);

    return gridOfPoints;
}

void writeToDotFile(Lattice& lattice, std::string& fileName) {
    std::ofstream dotFile;
    dotFile.open(fileName);
    std::string fileContents = "";
    fileContents += "digraph {\n";
    fileContents += "graph [pad=\"0.212, 0.055\" bgcolor=lightgray]\n";
    fileContents += "node [style=filled]\n";
       
    //dotFile << "digraph {\n";
    //dotFile << "graph [pad=\"0.212, 0.055\" bgcolor=lightgray]\n";
    //dotFile << "node [style=filled]\n";
    auto nodeList = lattice.getNodeList();

    for (auto const& node : nodeList) {
        fileContents += std::to_string(node.getNodeId()) + " [fillcolor=blue pos=\"" + std::to_string(node.getLocation().x) + "," + std::to_string(node.getLocation().y) + "!\"]\n";

    }
    //    a->b
    //    a->c
    //    a->d
    for (auto const& entry : lattice.getAdjacencyList()) {
        for (auto const& target_node : entry.second) {
            fileContents += std::to_string(entry.first.getNodeId()) + " -> " + std::to_string(target_node.getNodeId()) + "\n";
        }
    }

    fileContents += "}\n";
    //std::cout << fileContents;
    dotFile << fileContents;
    //dotFile << "}\n";
    dotFile.close();
}

void writeToJson(const Lattice& lattice, const std::string& fileName) {
    std::ofstream jsonFile;
    jsonFile.open(fileName);
    std::string fileContents = "";
    fileContents += "{\n";
    fileContents += "\"digraph\" : {\n";

    auto nodeList = lattice.getNodeList();

    fileContents += "\"nodes\" : [";
    for (auto const& node : nodeList) {
        fileContents += "\n{\n";
        fileContents += "\"" + std::to_string(+ node.getNodeId()) + "\"" + ": [" + std::to_string(node.getLocation().x) + "," + std::to_string(node.getLocation().y) + "]\n},";
    }
    fileContents.pop_back();
    fileContents += "\n],\n";

    fileContents += "\"edges\" : [";
    for (auto const& entry : lattice.getAdjacencyList()) {
        for (auto const& target_node : entry.second) {
            fileContents += "\n{\n";
            fileContents += "\"" + std::to_string(entry.first.getNodeId()) + "\"" + ":" + "\"" + std::to_string(target_node.getNodeId()) + "\"";
            fileContents += "\n},";
        }
    }
    fileContents.pop_back();
    fileContents += "\n]\n";

    fileContents += "}\n";
    fileContents += "}";
    jsonFile << fileContents;
    jsonFile.close();
}

void writeToFile(Lattice& lattice, std::string& fileName) {
    //writeToDotFile(lattice, fileName);
    writeToJson(lattice, fileName);
}

int main() {
    
    // Need a lattice
    // Center of lattice is the person/entity
    // Initialization will determine whether the entity gets initialized at the center or not
    // Define a radius for calculating effective happiness
    // How is this measured?

    // Each entity has some attributes
    // There is a type
    // There are some parameters associated with the entities (maybe type is one of them)
    // Do we need boost graph? We can make our own little impl.
    // We need a traversal of nodes between two specified nodes
    // shortest paths between all nodes? number of paths between node a and node b?
    
    auto orderedLattice = generateOrderedLattice();
    calculateOrder(orderedLattice);
    //std::string fileName = "orderedLattice.txt";
    std::string fileName = "orderedLattice.json";
    writeToFile(orderedLattice, fileName);

    auto disorderedLattice = generateDisorderedLattice();
    calculateOrder(disorderedLattice);
    //fileName = "disorderedLattice.txt";
    fileName = "disorderedLattice.json";
    writeToFile(disorderedLattice, fileName);
    
    return 0;
}
